import ASTNode from "./lib/ASTNode";
import useParser from "./lib/hooks/useParser";
import { SelectionContext } from "./lib/contexts/SelectionProvider";
import { useContext } from "react";

function App({ ast, meta, langId, isEmpty, level }) {
  const [tree] = useParser({
    ast,
    meta,
    langId,
    isEmpty,
  });

  const ctx = useContext(SelectionContext);

  return (
    <div className="App" style={{ width: 800, margin: "auto" }}>
      <ASTNode node={tree} level={level} ctx={ctx} />
    </div>
  );
}

export default App;
