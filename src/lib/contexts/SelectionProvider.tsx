import _forEach from 'lodash/forEach';
import { createContext, useCallback, useMemo, useState } from 'react';
import getNodeFromPoint from '../utils/getNodeFromPoint';
import isRootNode from '../utils/isRootNode';


export const SelectionContext = createContext({
  languages: {},
  source: null,
  target: null,
  sourceTree: null,
  targetTree: null,
  select: (n) => {
    console.error('[NYI] select by AST', n);
  },
  storeTree: (t, isTranspiler) => {
    console.error('[NYI] store tree', t, isTranspiler);
  },
  storeLanguages: (t) => {
    console.error('[NYI] store langs', t);
  },
});

export function SelectionProvider({ children }) {
  const [languages, setLanguages] = useState({});
  const [sourceTree, setSourceTree] = useState(null);
  const [targetTree, setTargetTree] = useState(null);
  const [source, setSource] = useState(null);
  const [target, setTarget] = useState(null);

  const select = useCallback(
    function select({
      node,
      from,
      isTranspiler = false,
      isTarget = false
    }) {
      // if no node select none
      if (!node) {
        setSource(null);
        setTarget(null);
        return;
      }

      const {
        key,
        path,
        inversePosition,
      } = node;

      // do not select root node
      if (isRootNode(path)) {
        return;
      }

      // do nothing is node is already selected
      const selection = isTarget ? target : source;
      if (selection?.key === key) {
        return;
      }

      // store node
      isTarget ? setTarget(node) : setSource(node);

      // get inverse node
      const inode = getNodeFromPoint({
        point: inversePosition,
        isTranspiler,
        isTarget: !isTarget,
        tree: isTarget ? targetTree : sourceTree,
        from,
      });

      // store inverse node
      if (isTarget) setSource(inode); else setTarget(inode);
    },
    [
      sourceTree,
      targetTree,
      source,
      target,
    ],
  );

  const storeTree = useCallback(
    function storeTree(tree, isTranspiler) {
      if (isTranspiler) {
        setSourceTree(tree);
      } else {
        setTargetTree(tree);
      }
    },
    [],
  );

  const storeLanguages = useCallback(
    function storeLanguages(langs) {
      const res = {};
      _forEach(langs, (row) => {
        res[row.id] = row.name;
      });
      setLanguages(res);
    },
    [],
  )

  // memoize values to skip re-renders
  const value = useMemo(
    () => ({
      source,
      target,
      sourceTree,
      targetTree,
      select,
      storeTree,
      languages,
      storeLanguages,
    }),
    [
      source,
      target,
      sourceTree,
      targetTree,
      select,
      storeTree,
      languages,
      storeLanguages,
    ],
  );

  return (
    <SelectionContext.Provider value={value}>
      { children }
    </SelectionContext.Provider>
  );
}
