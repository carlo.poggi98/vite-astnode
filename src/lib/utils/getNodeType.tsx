import _get from 'lodash/get';

function getNodeType(node): string|null {
  if (node.isStatementNode()){
    return 'Statement'
  }
  if (node.isExpressionNode()) {
    return 'Expression'
  }
  if (node.isDeclarationNode()) {
    return 'Declaration'
  }
  return null;
}

export default getNodeType;
