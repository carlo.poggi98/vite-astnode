// returns the opposite of the current node
// from source to target use sourceNode.getDestinationNode().destination
// from target to source use targetNode.getSourceNode().position
// includes also the key for AST node checks

function getInverse({ node, isTranspiler, isTarget }) {
  if (!node || !isTranspiler) {
    return {
      inversePosition: null,
      inverseKey: null,
    };
  }

  const getter = isTarget ? node?.getSourceNode : node?.getDestinationNode;

  let res;
  let ik = "";
  let ip = null;

  try {
    res = getter.apply(node);
  } catch (e) {
    return {
      inversePosition: null,
      inverseKey: null,
    };
  }

  try {
    const title = res.getRole();
    const eClassName = res.getSimpleType();
    const path = res.getPathFromRoot().join();
    ik = `${path}.${eClassName}.${title}`;
  } catch (e) {
    // console.warn('error looking for inverse key');
  }

  try {
    ip = isTarget ? res?.getPosition() : res?.getDestination();
  } catch (e) {
    console.warn("error looking for inverse position");
  }

  return {
    inversePosition: ip,
    inverseKey: ik,
  };
}

export default getInverse;
