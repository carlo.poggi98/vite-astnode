import _map from "lodash/map";
import _filter from "lodash/filter";
// import _toString from 'lodash/toString';
// import _isFunction from 'lodash/isFunction';
import isNode from "./isNode";
import getInverse from "./getInverse";
import getPosition from "./getPosition";
import getAttributes from "./getAttributes";

function getChildrens({ node, level, isTranspiler, isTarget }) {
  const children = node.getChildren();
  if (!children) {
    return [];
  }

  return _filter(
    _map(
      children,
      // eslint-disable-next-line no-use-before-define
      (child) =>
        getFields({
          node: child,
          level: level + 1,
          isTranspiler,
          isTarget,
        })
    )
  );
}

function getFields(args) {
  const {
    node,
    level = 0,
    recursive = true,
    isTranspiler = false,
    isTarget = false,
    from,
  } = args ?? {};

  if (!isNode(node)) {
    return null;
  }
  const title = node.getRole();
  const eClassName = node.getSimpleType();
  const path = node.getPathFromRoot().join();
  const key = `${path}.${eClassName}.${title}`;
  const attributes = getAttributes(node);
  const position = getPosition(node);
  const { inverseKey, inversePosition } = getInverse({
    node,
    isTranspiler,
    isTarget,
  });

  const childrens = recursive
    ? getChildrens({
        node,
        level,
        isTranspiler,
        isTarget,
      })
    : [];

  return {
    key,
    title,
    path,
    eClassName,
    attributes,
    childrens,
    position,
    inversePosition,
    inverseKey,
    original: node,
    isTranspiler,
    from,
  };
}

export default getFields;
