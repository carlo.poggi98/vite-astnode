import _get from "lodash/get";
import _first from "lodash/first";

function getSelectionPath(selection = []) {
  return _first(_get(selection, "key", "").split("."));
}

export default getSelectionPath;
