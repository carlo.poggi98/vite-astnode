import docs from "../docs";
import icon from "./docs.svg";

function DocsLink(props) {
  const { langId, qualifiedName } = props;
  const lang = docs.DOCS_MAP[langId];

  if (!lang) {
    return null;
  }

  return (
    <a
      style={{
        margin: 5,
      }}
      href={`${docs.DOCS_URL}/${lang}/latest#${qualifiedName}`}
      target="blank"
    >
      <img src={icon} alt="documentation" height={20} width={20} />
    </a>
  );
}

export default DocsLink;
