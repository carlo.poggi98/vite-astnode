import { findByPosition } from "@strumenta/tylasu";
import getFields from "./getFields";
import { extractPosition } from "./positions";

function getNodeFromPoint({
  point,
  tree,
  isTranspiler,
  isTarget,
  from = "code",
}) {
  if (!point) {
    return null;
  }
  const pos = extractPosition(point);
  const node = findByPosition(tree, pos);
  const fields = getFields({
    node,
    recursive: false,
    isTranspiler,
    isTarget,
    from,
  });

  return fields;
}

export default getNodeFromPoint;
