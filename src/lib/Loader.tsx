function Loader(props) {
  const { loading, error } = props;
  if (!loading && !error) {
    return null;
  }

  return (
    <div className="panel-block">
      {loading && <div>Loading, please wait... </div>}
      {error && <div>There is a problem fetching the data -{error}</div>}
    </div>
  );
}

export default Loader;
