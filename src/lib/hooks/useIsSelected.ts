import { useMemo } from 'react';

function useIsSelected(args) {
  const {
    selection,
    nodeKey,
  } = args;

  const { key } = selection || {};
  const isSelected = useMemo(
    () => (selection && key === nodeKey),
    [key, nodeKey, selection]
  )

  return isSelected;
}

export default useIsSelected;
