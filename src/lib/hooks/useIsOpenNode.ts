import { useEffect, useState } from "react";

function getIsOpen(level) {
  const DEFAULT_OPEN_ABOVE_LEVEL = 2;
  return level < DEFAULT_OPEN_ABOVE_LEVEL;
}

function useIsOpenNode(args) {
  const { node, path, level, selection, isSelected } = args ?? {};
  const [isOpen, setOpen] = useState(getIsOpen(level));

  useEffect(() => {
    // if no selection applied, all grey and open
    if (!selection) {
      setOpen(getIsOpen(level));
      return;
    }

    let isInSelectionPath;
    // path nodes to selection must be kept open
    if (!isSelected) {
      const selPath = selection?.key || "N/A";
      isInSelectionPath = selPath.substring(0, path.length) === path;
    }
    setOpen(isInSelectionPath || isSelected);
  }, [node, selection, isSelected, path, level]);

  return [isOpen, setOpen] as const;
}

export default useIsOpenNode;
