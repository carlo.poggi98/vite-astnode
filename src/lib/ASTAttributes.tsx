import _map from "lodash/map";
import _isEmpty from "lodash/isEmpty";
import { getChildAttributes } from "./utils/ast.strumenta";
import style from "./ASTAttributes.module.css";

function ASTAttributes(props) {
  const { id, attributes = [], childrens } = props;

  const childAttributes = getChildAttributes(childrens);

  if (_isEmpty([...attributes, ...childAttributes])) {
    return null;
  }

  return (
    <div className={style.container}>
      <table className={style.attributes}>
        <tbody>
          {_map(attributes, (a, index) => (
            <tr key={`${id}__attr.${a.key}.${index}`}>
              <td className={style.keyCell}>{a.key}:</td>
              <td>{a.value}</td>
            </tr>
          ))}
          {_map(childAttributes, (c, index) => (
            <tr key={`${id}__child.${c.key}.${index}`}>
              <td className={style.keyCell}>{c.key}</td>
              <td>{c.value}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ASTAttributes;
