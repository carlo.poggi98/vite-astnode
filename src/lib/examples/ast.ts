export default {
  name: "hello",
  code: "BEGIN\ndbms_output.put_line ('Hello World..');\nEND;\n/",
  ast: {
    eClass: "https://strumenta.com/kolasu/v1#//Result",
    root: {
      eClass: "#//CompilationUnit",
      position: {
        start: {
          line: 1,
        },
        end: {
          line: 4,
          column: 1,
        },
      },
      script: [
        {
          eClass: "#//AnonymousBlock",
          position: {
            start: {
              line: 1,
            },
            end: {
              line: 3,
              column: 3,
            },
          },
          seq_of_statements: [
            {
              eClass: "#//RoutineCall",
              position: {
                start: {
                  line: 2,
                },
                end: {
                  line: 2,
                  column: 38,
                },
              },
              calls: [
                {
                  position: {
                    start: {
                      line: 2,
                    },
                    end: {
                      line: 2,
                      column: 38,
                    },
                  },
                  arguments: {
                    position: {
                      start: {
                        line: 2,
                        column: 21,
                      },
                      end: {
                        line: 2,
                        column: 38,
                      },
                    },
                    argument: [
                      {
                        position: {
                          start: {
                            line: 2,
                            column: 22,
                          },
                          end: {
                            line: 2,
                            column: 37,
                          },
                        },
                        argument: {
                          eClass: "#//StringLiteral",
                          position: {
                            start: {
                              line: 2,
                              column: 22,
                            },
                            end: {
                              line: 2,
                              column: 37,
                            },
                          },
                          value: "Hello World..",
                        },
                      },
                    ],
                  },
                  routine_name: {
                    position: {
                      start: {
                        line: 2,
                      },
                      end: {
                        line: 2,
                        column: 20,
                      },
                    },
                    names: [
                      {
                        position: {
                          start: {
                            line: 2,
                          },
                          end: {
                            line: 2,
                            column: 11,
                          },
                        },
                        id: "dbms_output",
                      },
                      {
                        position: {
                          start: {
                            line: 2,
                            column: 12,
                          },
                          end: {
                            line: 2,
                            column: 20,
                          },
                        },
                        id: "put_line",
                      },
                    ],
                  },
                },
              ],
            },
          ],
        },
        {
          eClass: "#//SqlPlusCommand",
          position: {
            start: {
              line: 4,
            },
            end: {
              line: 4,
              column: 1,
            },
          },
          text: "/",
        },
      ],
    },
  },
  parsingTime: 1221,
  astBuildingTime: 1264,
};
