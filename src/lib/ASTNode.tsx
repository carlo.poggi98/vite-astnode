import { useRef } from "react";
import _map from "lodash/map";
import _noop from 'lodash/noop'
import ASTAttributes from "./ASTAttributes";
import Loader from "./Loader";
import useColoredNode from "./hooks/useColoredNode";
import useIsSelected from "./hooks/useIsSelected";
import useScrollToNode from "./hooks/useScrollToNode";
import useIsOpenNode from "./hooks/useIsOpenNode";
import isRootNode from "./utils/isRootNode";
import style from "./ASTNode.module.css";
import DocsLink from './utils/DocsLink';
import NodeType from "./utils/NodeType";
import { useParams } from "react-router-dom";

function ASTNode(props) {
  const {
    node,
    level,
    viewNode = _noop,
    isTarget = false,
    isTranspiler = false,
    titleOverride,
    ctx = {},
    hasDocs = false,
  } = props;

  const {
    key,
    path,
    title,
    eClassName,
    attributes,
    childrens,
    inversePosition,
    type,
    qualifiedName,
  } = node || {};

  const nodeRef = useRef(null);

  const { source, target, select } = ctx;
  const selection = isTarget ? target : source;

  const { langId } = useParams();

  // determines if the current node is selected
  const isSelected = useIsSelected({
    selection,
    nodeKey: key,
  });

  // scroll to current node if selected
  useScrollToNode({
    nodeRef,
    selection,
    isSelected,
  });

  // color nodes based on selection level
  const colorClass = useColoredNode({
    node,
    path,
    level,
    selection,
    isSelected,
  });

  // open/close nodes based on selection status
  const [isOpen, setOpen] = useIsOpenNode({
    node,
    path,
    level,
    selection,
    isSelected,
  });

  // determines if this is the root node
  const isRoot = isRootNode(path);

  if (!node) {
    return <Loader loading />;
  }

  const onSelect = () => {
    select({
      node: isSelected || isRoot ? null : node,
      isTarget,
      isTranspiler,
      from: "AST",
    });
  };

  const onViewNode = () => {
    select({
      node: isRoot ? null : node,
      isTarget,
      isTranspiler,
      from: "AST",
    });
    viewNode(inversePosition);
  };

  const toggleOpen = () => {
    setOpen((prev) => !prev);
  };

  const header = (
    <div
      className={`${colorClass} ${style.panelHeading}`}
      style={{
        display: "flex",
        justifyContent: "space-between",
      }}
      onClick={toggleOpen}
    >
      {isRoot ? (
        <div />
      ) : (
        <button type="button" className={style.selectButton} onClick={onSelect}>
          {isSelected ? "De-select" : "Select"}
        </button>
      )}
      <h6 className={style.title}>{titleOverride || title}</h6>
      {inversePosition ? (
        <button
          type="button"
          className={style.selectButton}
          onClick={onViewNode}
        >
          {isTarget ? "Go to original code" : "Go to transpiled code"}
        </button>
      ) : (
        <div />
      )}
      {/* <Button
        rounded
        size="small"
      >
        {isOpen ? '-' : '+'}
      </Button> */}
    </div>
  );

  if (!isOpen) {
    return (
      <div
        className={`pl-5 astnode ${isSelected ? "selection" : ""}`}
        ref={nodeRef}
      >
        <div className={style.header}>{header}</div>
      </div>
    );
  }

  return (
    <div
      key={key}
      className={`${style.selected} ${isSelected ? "selection" : ""}`}
      ref={nodeRef}
    >
      <div className={style.header}>
        {header}
        <div className={style.panelBlock}>
          <NodeType type={type} />
          <strong>{eClassName}</strong>
          {hasDocs && <DocsLink langId={langId} qualifiedName={qualifiedName} />}
        </div>
        <ASTAttributes id={key} attributes={attributes} childrens={childrens} />
      </div>
      {_map(childrens, (child, i) => (
        <div key={`${key}[${i}]`}>
          <ASTNode
            node={child}
            level={level + 1}
            isTarget={isTarget}
            isTranspiler={isTranspiler}
            viewNode={viewNode}
            ctx={ctx}
          />
        </div>
      ))}
    </div>
  );
}

export default ASTNode;
