export type ExampleFile = {
  path: string;
  result: object;
  code: object;
};

export type TranspilerExample = {
  originalCode: string;
  generatedCode: string;
  issues: object;
  // hasMultipleFiles: true
  generatedFiles?: ExampleFile[];
  originalFiles?: ExampleFile[];
  // hasMultipleFiles: false
  sourceResult?: object;
  targetResult?: object;
};

export type Transpiler = {
  name: string;
  srcName: string;
  srcMeta: unknown;
  targetName: string;
  targetMeta: unknown;
  hasDocs: boolean;
  hasMultipleFiles: boolean;
  example: TranspilerExample;
};
