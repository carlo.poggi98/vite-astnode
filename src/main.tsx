import React from "react";
import ReactDOM from "react-dom/client";
import { SelectionProvider } from "./lib/contexts/SelectionProvider";
import App from "./App";
import metadata from "./lib/examples/meta";
import ast from "./lib/examples/ast";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <SelectionProvider>
      <App ast={ast} meta={metadata} langId="cobol" isEmpty={false} level={0} />
    </SelectionProvider>
  </React.StrictMode>
);
