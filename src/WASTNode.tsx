import ASTNode from "./lib/ASTNode";
import useParser from "./lib/hooks/useParser";

function WASTNode({ ast, meta, langId, isEmpty, level }) {
  const [tree] = useParser({
    ast,
    meta,
    langId,
    isEmpty,
  });

  return (
    <div className="App" style={{ width: 800, margin: "auto" }}>
      <ASTNode node={tree} level={level} />
    </div>
  );
}

export default WASTNode;
